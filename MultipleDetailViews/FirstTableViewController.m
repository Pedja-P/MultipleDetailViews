
/*
     File: FirstTableViewController.m
 Abstract: A table view controller that manages three rows. Selecting 
 the first row pushes SecondTableViewController onto the navigation 
 stack.  Selecting one of the remaining two rows creates a new detail 
 view controller that is added to the split view controller.
 
  Version: 1.2
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2012 Apple Inc. All Rights Reserved.
 
 */

#import "FirstTableViewController.h"
#import "DetailViewManager.h"
#import "FirstDetailViewController.h"

@implementation FirstTableViewController

#pragma mark - Table view data source

// -------------------------------------------------------------------------------
//	tableView:numberOfRowsInSection:
// -------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section 
{
    // Three rows.
    return 3;
}

// -------------------------------------------------------------------------------
//	tableView:cellForRowAtIndexPath:
// -------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // using manual segues (connected in Xcode from VC, not from cell, so there can be more than one)
    // and, therefore, we only need one prototype cell in the First table view
    static NSString *CellIdentifier = @"FirstTableCell";
    
    // Dequeue or create a cell of the appropriate type.
    // When cell is set in storyboard, no need for registering class in code
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSUInteger row = indexPath.row;
    
    // Set appropriate labels for the cells (switch is at least as fast as if-elseif)
    switch (row) {
            
        case 0:
            cell.textLabel.text = @"Second Table View";
            break;
            
        case 1:
            cell.textLabel.text = @"First Detail View 1";
            break;
            
        case 2:
            cell.textLabel.text = @"First Detail View 2";
            break;
            
        default:
            break;
    }
    return cell;
}

#pragma mark - Table view delegate

// -------------------------------------------------------------------------------
//	tableView:willDisplayCell:forRowAtIndexPath:
// -------------------------------------------------------------------------------
// If we want to change the background color of a cell (by setting the background color of a cell
// via the backgroundColor property declared by UIView) we must do it in the tableView:willDisplayCell:forRowAtIndexPath:
// method of the delegate and not in tableView:cellForRowAtIndexPath: of the data source.
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = indexPath.row;
    
    switch (row) {
            
        case 1:
            cell.backgroundColor = [UIColor purpleColor];
            cell.textLabel.textColor = [UIColor whiteColor];
            break;
            
        case 2:
            cell.backgroundColor = [UIColor orangeColor];
            
        default:
            break;
    }
}

// -------------------------------------------------------------------------------
//	tableView:didSelectRowAtIndexPath:
// -------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSUInteger row = indexPath.row;
    UITableViewCell *senderCell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (row == 0) {
        // Second table view
        [self performSegueWithIdentifier:@"ShowSecondTableView" sender:senderCell];
    }
    else {
        // First detail view
        if (self.splitViewController) { // on iPad
            // Create and configure a new detail view controller appropriate for the selection.
            UIViewController<SubstitutableDetailViewController> *detailViewController;
            FirstDetailViewController *fdvc =
                (FirstDetailViewController *)[self instantiateViewControllerWithIdentifier:@"FirstDetailView"
                                                                    fromStoryboardWithName:@"MainStoryboard_iPad"
                                                                                    bundle:nil];
            detailViewController = fdvc;
            detailViewController.title = senderCell.textLabel.text;
            detailViewController.view.backgroundColor = senderCell.backgroundColor;
            
            // Get a reference to the DetailViewManager.
            // DetailViewManager is the delegate of our split view.
            DetailViewManager *detailManager = (DetailViewManager *)self.splitViewController.delegate;
            
            // DetailViewManager exposes a property, detailViewController.  Set this property
            // to the detail view controller we want displayed.  Configuring the detail view
            // controller to display the navigation button (if needed) and presenting it
            // happens inside DetailViewManager.
            detailManager.detailViewController = detailViewController;
        }
        else {  // on iPhone
            [self performSegueWithIdentifier:@"ShowFirstDetailView" sender:senderCell];
        }
    }
}

#pragma mark - Prepare for segue

// -------------------------------------------------------------------------------
//	prepareForSegue:sender:
// -------------------------------------------------------------------------------
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowFirstDetailView"]) {
        if ([sender isKindOfClass:[UITableViewCell class]]) {
            UITableViewCell *senderCell = (UITableViewCell *)sender;
            ((UIViewController *)segue.destinationViewController).title = senderCell.textLabel.text;
            ((UIViewController *)segue.destinationViewController).view.backgroundColor = senderCell.backgroundColor;
        }
    }
}

#pragma mark - Instantiate view controller from storyboard

// -------------------------------------------------------------------------------
//	instantiateViewControllerWithIdentifier:fromStoryboardWithName:bundle:
//  helper method
// -------------------------------------------------------------------------------
- (id)instantiateViewControllerWithIdentifier:(NSString *)identifier
                       fromStoryboardWithName:(NSString *)name
                                       bundle:(NSBundle *)storyBoardBundle
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:name bundle:storyBoardBundle];
    // it could be done like this too: UIStoryboard *sb = self.storyboard; self is from same storyboard
    // as new VC
    
    id viewController = [sb instantiateViewControllerWithIdentifier:identifier];
    return viewController;
}

@end
