
/*
     File: SecondDetailViewController.m
 Abstract: A simple view controller that adopts the SubstitutableDetailViewController protocol defined by DetailViewManager.
 It's responsible for adding and removing the popover button in response to rotation. This view controller uses a toolbar.
 
  Version: 1.2
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2012 Apple Inc. All Rights Reserved.
 
 */

#import "SecondDetailViewController.h"

@interface SecondDetailViewController ()

@property (nonatomic, weak) IBOutlet UIBarButtonItem *titleBarButton;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIToolbar *toolbar;

@end

@implementation SecondDetailViewController

// -------------------------------------------------------------------------------
//	setTitle:
//  Custom implementation for the title setter, overriding superclass's
// -------------------------------------------------------------------------------
- (void)setTitle:(NSString *)title
{
    super.title = title;
    self.titleLabel.text = title;
    self.navigationItem.title = title;  // this one does the trick when running inside a UINavigationController (e.g. on an iPhone)
    self.titleBarButton.title = title;
}

#pragma mark - SubstitutableDetailViewController

// -------------------------------------------------------------------------------
//	setSplitViewBarButtonItem:
//  Custom implementation for the splitViewBarButtonItem setter.
//  In addition to updating the _setSplitViewBarButtonItem ivar, it
//  reconfigures the navigationBar to either show or hide the 
//  splitViewBarButtonItem.
// -------------------------------------------------------------------------------
- (void)setSplitViewBarButtonItem:(UIBarButtonItem *)splitViewBarButtonItem
{
    if (splitViewBarButtonItem != _splitViewBarButtonItem) {    // drawing is slow, so don't draw new button if it hasn't changed
        [self handleSplitViewBarButtonItem:splitViewBarButtonItem];    
    }
}

// ------------------------------------------------------------------------------------------
//	handleSplitViewBarButtonItem:
//  helper method - puts the splitViewBarButton in our toolbar (and/or removes the old one).
//  Must be called when our splitViewBarButtonItem property changes
// (and also after our view has been loaded from the storyboard - viewDidLoad).
// -------------------------------------------------------------------------------------------
- (void)handleSplitViewBarButtonItem:(UIBarButtonItem *)splitViewBarButtonItem
{
    NSMutableArray *toolbarItems = [self.toolbar.items mutableCopy];
    if (_splitViewBarButtonItem) [toolbarItems removeObject:_splitViewBarButtonItem];
    if (splitViewBarButtonItem) [toolbarItems insertObject:splitViewBarButtonItem atIndex:0];
    self.toolbar.items = toolbarItems;
    _splitViewBarButtonItem = splitViewBarButtonItem;
}

#pragma mark - View lifecycle

// -------------------------------------------------------------------------------
//	viewDidLoad:
// -------------------------------------------------------------------------------
- (void)viewDidLoad
{
    // -setSplitViewBarButtonItem (and setTitle) may have been invoked when before the
    // interface was loaded.  This will occur when setSplitViewBarButtonItem
    // is called as part of DetailViewManager preparing this view controller
    // for presentation as this is before the view is unarchived from the storyboard.
    // When viewidLoad is invoked, the interface is loaded and hooked up.
    // Check if we are supposed to be displaying a splitViewBarButtonItem
    // and if so, add it to the toolbar.
    [super viewDidLoad];
    [self handleSplitViewBarButtonItem:self.splitViewBarButtonItem];
    self.titleLabel.text = self.title;
    self.navigationItem.title = self.title;
    self.titleBarButton.title = self.title;
}

@end
