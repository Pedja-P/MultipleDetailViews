
/*
     File: SecondTableViewController.m
 Abstract: A table view controller that manages two rows. Selecting 
 a row creates a new detail view controller that is added to the split 
 view controller.
 
  Version: 1.2
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2012 Apple Inc. All Rights Reserved.
 
 */

#import "SecondTableViewController.h"
#import "DetailViewManager.h"
#import "SecondDetailViewController.h"
#import "FirstDetailViewController.h"

@implementation SecondTableViewController

#pragma mark - Table view data source

// -------------------------------------------------------------------------------
//	tableView:numberOfRowsInSection:
// -------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section 
{
    // Two rows.
    return 2;
}

// -------------------------------------------------------------------------------
//	tableView:cellForRowAtIndexPath:
// -------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    // using manual segues (connected in Xcode from VC, not from cell, so there can be more than one)
    // and, therefore, we only need one prototype cell in the Second table view
    static NSString *CellIdentifier = @"SecondTableCell";
    
    // Dequeue or create a cell of the appropriate type.
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSUInteger row = indexPath.row;
    // Set appropriate labels for the cells.
    switch (row) {
            
        case 0:
            cell.textLabel.text = @"First Detail View";
            break;
            
        case 1:
            cell.textLabel.text = @"Second Detail View";
            break;
            
        default:
            break;
    }
    return cell;
}

#pragma mark - Table view selection

// -------------------------------------------------------------------------------
//	tableView:didSelectRowAtIndexPath:
// -------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    // Get a reference to the DetailViewManager.  
    // DetailViewManager is the delegate of our split view.
    DetailViewManager *detailViewManager = (DetailViewManager*)self.splitViewController.delegate;
    
    // Create and configure a new detail view controller appropriate for the selection.
    UIViewController<SubstitutableDetailViewController> *detailViewController;
    
    NSUInteger row = indexPath.row;
    UITableViewCell *senderCell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (row == 0) {
        // First detail view
        if (self.splitViewController) { // on iPad
            FirstDetailViewController *fdvc =
                (FirstDetailViewController *)[self instantiateViewControllerWithIdentifier:@"FirstDetailView"
                                                                    fromStoryboardWithName:@"MainStoryboard_iPad"
                                                                                    bundle:nil];
            detailViewController = fdvc;
            detailViewController.title = senderCell.textLabel.text;
        }
        else {  // on iPhone
            [self performSegueWithIdentifier:@"ShowFirstDetailView" sender:senderCell];
        }
    }
    else {
        // Second detail view
        if (self.splitViewController) { // on iPad
            SecondDetailViewController *sdvc =
                (SecondDetailViewController *)[self instantiateViewControllerWithIdentifier:@"SecondDetailView"
                                                                     fromStoryboardWithName:@"MainStoryboard_iPad"
                                                                                     bundle:nil];
            detailViewController = sdvc;
            detailViewController.title = senderCell.textLabel.text;
        }
        else {  // on iPhone
            [self performSegueWithIdentifier:@"ShowSecondDetailView" sender:senderCell];
        }
    }
    // DetailViewManager exposes a property, detailViewController.  Set this property
    // to the detail view controller we want displayed.  Configuring the detail view
    // controller to display the navigation button (if needed) and presenting it
    // happens inside DetailViewManager.
    detailViewManager.detailViewController = detailViewController;
}

#pragma mark - Instantiate view controller from storyboard

// -------------------------------------------------------------------------------
//	instantiateViewControllerWithIdentifier:fromStoryboardWithName:bundle:
//  helper method
// -------------------------------------------------------------------------------
- (id)instantiateViewControllerWithIdentifier:(NSString *)identifier
                       fromStoryboardWithName:(NSString *)name
                                       bundle:(NSBundle *)storyBoardBundle
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:name bundle:storyBoardBundle];
    // it could be done like this too: UIStoryboard *sb = self.storyboard; self is from same storyboard
    // as new VC

    id viewController = [sb instantiateViewControllerWithIdentifier:identifier];
    return viewController;
}

#pragma mark - Prepare for segue

// -------------------------------------------------------------------------------
//	prepareForSegue:sender:
// -------------------------------------------------------------------------------
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowFirstDetailView"] ||
        [segue.identifier isEqualToString:@"ShowSecondDetailView"]) {
        if ([sender isKindOfClass:[UITableViewCell class]]) {
            UITableViewCell *senderCell = (UITableViewCell *)sender;
            ((UIViewController *)segue.destinationViewController).title = senderCell.textLabel.text;
        }
    }
}

@end
