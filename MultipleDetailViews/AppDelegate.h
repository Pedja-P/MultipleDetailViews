//
//  AppDelegate.h
//  MultipleDetailViews
//
//  Created by Predrag Pavlovic on 8/23/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

@import UIKit;
@class DetailViewManager;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window; 
@property (strong, nonatomic) DetailViewManager *detailViewManager;

@end
