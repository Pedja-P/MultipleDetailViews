//
//  main.m
//  MultipleDetailViews
//
//  Created by Predrag Pavlovic on 8/23/13.
//  Copyright (c) 2013 Rezedo. All rights reserved.
//

@import UIKit;

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
